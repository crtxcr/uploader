# uploader
This quickly hacked upload solution is my quite simple (of course), convenient CLI uploader, that also encrypts files. Send the link and the browser will decrypt it. Alternatively, you can also choose not to encrypt.

A cronjob will get rid of old files. `

## Setup

To be written™.


## Usage

```
upload file 3days
```
File will be encrypted before upload, and then an expiration entry created. Then you'll get the link. 

The file will expire after 3 days. Then the cronjob will remove it. 

```
upload file 10weeks plain
```

Same as above, but will expire after 10weeks and no encryption will happen. 

## Credits
Thanks to [lawl](https://github.com/lawl) for his work on [dumpaste](https://github.com/lawl/dumpaste), which I have stolen and modified here (with permission). His approach has a convenient upload form, a pastebin textbox and curl based CLI upload too. Expiration and uploads are handled by a go binary. 